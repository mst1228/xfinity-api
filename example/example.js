const Xfinity = require('../lib/xfinity')

const USERNAME = process.env['XFINITY_USERNAME']
const PASSWORD = process.env['XFINITY_PASSWORD']
if (!USERNAME || !PASSWORD) {
  console.log('Xfinity credentials required')
  process.exit(1)
}

const xfinity = new Xfinity()

xfinity.authenticate(USERNAME, PASSWORD)
  .then(x => {
    return xfinity.getDevices()
  })
  .then(data => {
    console.log(JSON.stringify(data))
  })
  .catch(error => {
    console.log(error)
  })
