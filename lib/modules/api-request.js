const request = require('request-promise')
const url = require('url')
const authentication = require('./authentication')

const API_URL_BASE = 'https://xtvapi.tvremote.comcast.net'

module.exports = function (endpoint) {
  if (typeof endpoint !== 'string') {
    throw new Error('api request endpoint must be a string')
  } else {
    let uri = url.resolve(API_URL_BASE, endpoint)
    if (uri[uri.lengh - 1] !== '/') uri += '/'

    return {
      authenticatedGet: function (auth, qs = {}) {
        if (typeof auth.access_token !== 'string') {
          throw new Error('access_token is required for authenticatedGet')
        } else {
          return authenticatedRequest({uri, qs, method: 'GET'}, auth)
        }
      },
      authenticatedPost: function (auth, form = {}) {
        if (typeof auth.access_token !== 'string') {
          throw new Error('access_token is required for authenticatedPost')
        } else {
          return authenticatedRequest({uri, form, method: 'POST'}, auth)
        }
      }
    }
  }
}

function authenticatedRequest (opts, auth) {
  opts.json = true
  opts.simple = false
  opts.resolveWithFullResponse = true
  opts.headers = {Accept: '*/*'}
  opts.auth = {bearer: auth.access_token}
  return request(opts)
    .then(response => {
      if (response.statusCode >= 200 && response.statusCode < 300) {
        return response.body
      } else if (response.statusCode === 403) {
        return authentication.refreshToken(auth)
          .then(data => {
            auth = data
            opts.auth = {bearer: auth.access_token}
            opts.simple = true
            opts.resolveWithFullResponse = false
            return request(opts)
          })
      } else {
        throw new AuthenticatedRequestError(response)
      }
    })
}

function AuthenticatedRequestError (response) {
  this.message = 'Error in response to authenticated request'
  this.name = 'AuthenticatedRequestError'
  this.response = response
}
