const request = require('request-promise')
const url = require('url')

// Sign in and get auth token
exports.getToken = function (username, password) {
  return request.post('https://login.comcast.net/login', {
    json: true,
    resolveWithFullResponse: true,
    simple: false,
    form: {
      user: username,
      passwd: password,
      client_id: 'tv-remote',
      s: 'oauth',
      continue: 'https://login.comcast.net/oauth/authorize?client_id=tv-remote&response_type=code&redirect_uri=xtvrauth://auth&prompt=login&response=1',
      lang: 'en'
    }
  }).then(response => {
    if (response.statusCode !== 302 || typeof response.headers.location !== 'string') {
      throw new Error(`Error logging in, step 1. StatusCode: ${response.statusCode}`)
    } else {
      return request.get(response.headers.location, {
        resolveWithFullResponse: true,
        simple: false,
        followRedirect: false
      })
    }
  }).then(response => {
    if (response.statusCode !== 302 || typeof response.headers.location !== 'string') {
      throw new Error(`Error logging in, step 2. StatusCode: ${response.statusCode}`)
    } else {
      const parsed = url.parse(response.headers.location, true)
      if (!parsed.query || !parsed.query.code) {
        throw new Error(`Error parsing step 2 URL: ${response.headers.location}`)
      } else {
        return request.post('https://login.comcast.net/oauth/token', {
          json: true,
          form: {
            client_id: 'tv-remote',
            client_secret: '0612aade0ded28ad5624b33d915e668c45d6ad69',
            code: parsed.query.code,
            grant_type: 'authorization_code',
            redirect_uri: `${parsed.protocol}//${parsed.host}`
          }
        })
      }
    }
  }).then(data => {
    if (typeof data.access_token !== 'string') {
      throw new Error(`Invalid value for access_token: ${data.access_token}`)
    } else {
      return data
    }
  })
}

exports.refreshToken = function (auth) {
  return request.post('https://login.comcast.net/oauth/token', {
    json: true,
    form: {
      client_id: 'tv-remote',
      client_secret: '0612aade0ded28ad5624b33d915e668c45d6ad69',
      grant_type: 'refresh_token',
      redirect_uri: 'xtvrauth://auth',
      refresh_token: auth.refresh_token
    }
  }).then(data => {
    if (typeof data.access_token !== 'string') {
      throw new Error(`Invalid value for access_token: ${data.access_token}`)
    } else {
      return data
    }
  })
}
