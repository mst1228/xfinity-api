const apiRequest = require('./api-request')

module.exports = function (id, auth) {
  if (typeof id !== 'string' || typeof auth.access_token !== 'string') {
    throw new Error('id of device and access_token must be a string')
  } else {
    return {
      getEntityRecordings: function () {
        return apiRequest(`devices/${id}/entityRecordings`).authenticatedGet(auth)
      },
      getScheduledRecordingsWithEntities: function () {
        return apiRequest(`devices/${id}/recordings/scheduledWithEntities`).authenticatedGet(auth)
      },
      getCompletedRecordings: function () {
        return apiRequest(`devices/${id}/recordings/completed`).authenticatedGet(auth)
      },
      deviceSummary: function () {
        return apiRequest(`devices/${id}/dvr/summary`).authenticatedGet(auth)
      },
      startVoiceSession: function () {
        return apiRequest(`devices/${id}/remote/voice/start-session`).authenticatedGet(auth)
      },
      issueVoiceCommand: function (voiceSessionToken, command) {
        return apiRequest('remote/voice/command').authenticatedPost(auth, {voiceSessionToken, command})
      },
      renameDevice: function (deviceName) {
        return apiRequest(`devices/${id}/rename`).authenticatedPost(auth, {deviceName})
      },
      tuneChannel: function (channelNumber) {
        return apiRequest(`devices/${id}/remote/tune/channel`).authenticatedPost(auth, {channelNumber})
      },
      tuneVOD: function (mediaId) {
        return apiRequest(`devices/${id}/remote/tune/vod`).authenticatedPost(auth, {mediaId})
      },
      tuneRecording: function (mediaId) {
        return apiRequest(`devices/${id}/remote/tune/recording`).authenticatedPost(auth, {mediaId})
      },
      processKey: function (keyCode) {
        return apiRequest(`devices/${id}/remote/processKey`).authenticatedPost(auth, {keyCode})
      },
      displayName: function (name) {
        return apiRequest(`devices/${id}/remote/displayName`).authenticatedPost(auth, (typeof name === 'string') ? {name} : undefined)
      },
      changePriorityList: function (seriesInPriority) {
        return apiRequest(`devices/${id}/entityRecordings`).authenticatedPost(auth, {seriesInPriority})
      }
    }
  }
}
