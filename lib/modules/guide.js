const moment = require('moment')
const apiRequest = require('./api-request')

module.exports = function (auth) {
  if (typeof auth.access_token !== 'string') {
    throw new Error('access_token must be a string')
  } else {
    return {
      getTVGridShape: function (startTime = moment().startOf('hour').valueOf()) {
        return apiRequest('tvgrid').authenticatedGet(auth, {startTime})
      },
      getTVGridChunk: function (startTime = moment().startOf('hour').valueOf(), hours = 6) {
        return apiRequest('tvgrid/chunks').authenticatedGet(auth, {startTime, hours})
      },
      getTVGridListing: function (programId, channelId) {
        return apiRequest(`tvgrid/listing/${programId}`).authenticatedGet(auth, {channelId})
      },
      searchByTerm: function (query, limit = 60) {
        return apiRequest('search/term').authenticatedGet(auth, {query, limit})
      }
    }
  }
}
