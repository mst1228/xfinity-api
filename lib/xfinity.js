const authentication = require('./modules/authentication')
const apiRequest = require('./modules/api-request')
const guide = require('./modules/guide')
const device = require('./modules/device')

class Xfinity {
  constructor () {
    this._auth = {}
  }

  authenticate (username, password) {
    return authentication.getToken(username, password)
      .then(auth => {
        this._auth = auth
        return this
      })
  }

  getChannelMap () {
    return apiRequest('channelmap').authenticatedGet(this._auth)
  }

  getFavorites () {
    return apiRequest('favorites').authenticatedGet(this._auth)
  }

  getGuideShape (startTime) {
    return guide(this._auth).getTVGridShape(startTime)
  }

  getGuideChunk (startTime, hours) {
    return guide(this._auth).getTVGridChunk(startTime, hours)
  }

  getProgramDetails (programId, channelId) {
    return guide(this._auth).getTVGridListing(programId, channelId)
  }

  searchProgramsByTerm (query, limit) {
    return guide(this._auth).searchByTerm(query, limit)
  }

  getDevices () {
    return apiRequest('devices').authenticatedGet(this._auth)
  }

  tuneDeviceToChannel (deviceId, channelNumber) {
    return device(deviceId, this._auth).tuneChannel(channelNumber)
  }

  pressKeyOnDevice (keyCode, deviceId) {
    return device(deviceId, this._auth).processKey(keyCode)
  }

  displayNameOnDevice (name, deviceId) {
    return device(deviceId, this._auth).displayName(name)
  }
}

module.exports = Xfinity
